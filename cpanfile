requires "Carp" => "0";
requires "File::Spec" => "0";
requires "File::Temp" => "0";
requires "PerlIO" => "0";
requires "Scalar::Util" => "0";
requires "Storable" => "0";
requires "perl" => "5.008001";
requires "utf8" => "0";

on 'configure' => sub {
  requires "ExtUtils::MakeMaker" => "0";
};

on 'develop' => sub {
  requires "Test::Pod" => "1.41";
};
